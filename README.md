# A simple counter-app using [React](https://reactjs.org)

> **This tiny application is like adding, removing, deleting and resetting products which reflects in the total number of products into our shopping cart.**

**This is a very basic app but useful to understand the React Components, states, data flow, parent to child etc.**

## Scripts

Install dependencies (node modules)

#### `npm i`

Run the project from the root directory

#### `npm start`

_Runs the app in the development mode.<br>_
[http://localhost:3000](http://localhost:3000)
