// import React, { Component } from "react";

// class SonarTest extends Component {
//   foo = () => {
//     // // Vulnerability
//     // localStorage.setItem("login", "fred.flintstone"); // Noncompliant
//     // // Code Smell
//     // let a = [1, 3, 6, 9];
//     // let c = [2, 4, 7, 10];
//     // var b = a.reverse(); // Noncompliant
//     // var d = c.sort(); // Noncompliant
//   };

//   bar = () => {
//     // // Bug
//     // var j = 0;
//     // while (true) {
//     //   // Noncompliant; constant end condition
//     //   j++;
//     // }
//   };

//   render() {
//     return <div>{this.foo()}</div>;
//   }
// }

// export default SonarTest;
