import React from "react";
//import ReactDOM from "react-dom";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import App from "./App";

// it("renders without crashing", () => {
//   const div = document.createElement("div");
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

describe("App component", () => {
  // it("starts with a count of 0", () => {
  //   const wrapper = shallow(<App />);
  //   const text = wrapper.find("Counter").text();
  //   expect(text).toEqual("0");
  // });

  // it("increments count by 1 when the increment button is clicked", () => {
  //   const wrapper = shallow(<App />);
  //   const incrementBtn = wrapper.find("button.increment");
  //   incrementBtn.simulate("click");
  //   const text = wrapper.find("p").text();
  //   expect(text).toEqual("Count: 1");
  // });
  // it("decrements count by 1 when the decrement button is clicked", () => {
  //   const wrapper = shallow(<App />);
  //   const decrementBtn = wrapper.find("button.decrement");
  //   decrementBtn.simulate("click");
  //   const text = wrapper.find("p").text();
  //   expect(text).toEqual("Count: -1");
  // });

  it("matches the snapshot", () => {
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

// TODO: https://jestjs.io/docs/en/tutorial-react
// TODO: https://www.vivekkalyan.com/achieving-test-coverage-react
