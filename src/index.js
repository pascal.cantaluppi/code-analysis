import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";

import * as Sentry from "@sentry/browser";

const RELEASE = "0.1.0";
Sentry.init({
  dsn:
    "https://" +
    process.env.REACT_APP_SENTRY_KEY1 +
    ".ingest.sentry.io/" +
    process.env.REACT_APP_SENTRY_KEY2,
  release: RELEASE,
});

ReactDOM.render(<App />, document.getElementById("root"));
